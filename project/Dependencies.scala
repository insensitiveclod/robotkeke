/*
 * Copyright (c) 2012 SnowPlow Analytics Ltd. All rights reserved.
 *
 * This program is licensed to you under the Apache License Version 2.0,
 * and you may not use this file except in compliance with the Apache License Version 2.0.
 * You may obtain a copy of the Apache License Version 2.0 at http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the Apache License Version 2.0 is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Apache License Version 2.0 for the specific language governing permissions and limitations there under.
 */
import sbt._

object Dependencies {
  val resolutionRepos = Seq(
//    ScalaToolsSnapshots,
    "Local Maven Repository" at "file://"+Path.userHome.absolutePath+"/.m2/repository",
    "Concurrent Maven Repo" at "http://conjars.org/repo", // For Scalding, Cascading etc
    "Typesafe" at "http://repo.typesafe.com/typesafe/repo"
  )

  object V {
 
    //val specs2    = "2.3.8" //"1.12.3" // -> "1.13" when we bump to Scala 2.10.0
    // Add versions for your additional libraries here...
    val guava 	 = "14.0"
  }

  object Libraries {
    val pickling	 	= "org.scala-lang" 				%% "scala-pickling" 			% "0.8.0-SNAPSHOT"
    val specs		 	= "org.scala-tools.testing"   	%% "specs" 				  		% "1.6.9" 	   		% "test"
    val guava			= "com.google.guava" 			% "guava" 						% V.guava
    val elephantbird	= "com.twitter.elephantbird"	% "elephant-bird"				% "4.4"
    val elephantbirdcas = "com.twitter.elephantbird"	% "elephant-bird-cascading2" 	% "4.4"

    // Add additional libraries from mvnrepository.com (SBT syntax) here...
    //val mahoutCore   = "org.apache.mahout"	    % "mahout-core"	      % V.mahout 
    //val mahoutMath   = "org.apache.mahout"	    % "mahout-math"	      % V.mahout 
    val scalatest 	= "org.scalatest" 				% "scalatest_2.10" % "1.9.1" % "test"
    val junit   	= "junit" 						% "junit" % "4.10" % "test"
    
    // Scala (test only)
    //val specs2       = "org.specs2"                 %% "specs2"               % V.specs2       % "test"
  }
  
  
}


import sbt._
import Keys._

object HelloWorldOpenBuild extends Build {

  import Dependencies._
  import BuildSettings._

  // Configure prompt to show current project
  override lazy val settings = super.settings :+ {
    shellPrompt := { s => Project.extract(s).currentProject.id + " > " }
  }

  // Define our project, with basic project information and library dependencies
  lazy val project = Project("hellowworldopen", file("."))
    .settings(buildSettings: _*)
    .settings(
      libraryDependencies ++= Seq(
        Libraries.scalatest,
        Libraries.junit,
	//Libraries.mahoutCore intransitive(),
	//Libraries.mahoutMath intransitive(),
        Libraries.specs
        // Add your additional libraries here (comma-separated)...
      )
    )//.dependsOn(depProject)
    
   //lazy val depProject = RootProject(uri("git://github.com/ParallelAI/SpyGlass.git#%s".format(V.spyglass)))
}

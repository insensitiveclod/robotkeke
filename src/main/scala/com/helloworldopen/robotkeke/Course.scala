package com.helloworldopen.robotkeke

//import scala.collection.mutable.Map
import com.helloworldopen.robotkeke.Types._

object Course {

  // Could probably represent this as Map[Int, Map[Int, Position] ]
  // x, 
  type CourseMap 	= Map[Int, Set[(Int,Int)]]
  //type CostMap		= Map[(Int,Int), Int]
  type CostMap		= Map[Int, Map[Int,Int]]
  
  /**
   * Get adjacent cells that are on the course
   */
  def neighbours(cm: CourseMap, pos: Position) : Set[Position] = {
    val (x, y) = pos

    val xIndexes = List(x - 1, x, x + 1)
    val adjacent = xIndexes.flatMap(cm.get).flatten

    val yIndexes = List(y - 1, y, y + 1)
    adjacent.filter(p => yIndexes.contains(p._2)) toSet
  } 
  
  /**
   * Need set the initial values to encourage the car to progress
   * in a forwards direction
   */
  def doCosting(cm: CourseMap)(frontier: Set[(Position, Int)], costed: Set[(Position, Int)]) : Set[(Position, Int)] = {

    val costedCells = costed.map(_._1) 
    
    // Assume that direction is counter clockwise
    val next = frontier.flatMap { e =>
      val (pos, cost) = e
      
      val n = neighbours(cm, pos)
        .filter(r =>  !costedCells.contains(r)) // don't backtrack
        .map(pos => (pos, cost + 10))
      n
    }
    //println(next)
    next
  }

  /**
   *
   */
  def generateInitialCosts(cm: CourseMap, startLine: Set[Position]) : CostMap = {


    def isEasterly(pos1: Position, pos2: Position): Boolean = pos2._1 - pos1._1 < 0

    val cellCount = cm.values.size

    val easterlyNeighbours = startLine.flatMap { e =>
      val n = neighbours(cm, e)
        .filter(r => isEasterly(r, e))
        .map(pos => (pos, Int.MaxValue / 2))
      n
    }

    val costedStartLine = startLine.map(pos => (pos, 0))			

    def gcImpl(frontier: Set[(Position, Int)], costed: Set[(Position, Int)]) : Set[(Position, Int)] = {
      
      if (frontier.isEmpty) costed
      else {
    	  val newFrontier = doCosting(cm)(frontier, costed)
    	  val newCosted = costed ++ newFrontier
    	  gcImpl(newFrontier, newCosted)
      }
    }

    val res = gcImpl(easterlyNeighbours, costedStartLine)

    val result = scala.collection.mutable.Map[Int, scala.collection.mutable.Map[Int, Int]]()

    val pe = res.foreach { p =>
      val (pos, cost) = p
      val value = result.getOrElse(pos._1, scala.collection.mutable.Map[Int, Int]())
      value(pos._2) = cost
      result += (pos._1 -> value)
    }

    result.map(kv => (kv._1, kv._2.toMap)).toMap
  }

  /**
   * Debug routine
   */
  def dump(cells : Iterable[Position]) = {
    val o2 = Array.tabulate(16, 32)((x, y) => cells.find(p => p == (y, x)))

    o2 foreach { e =>
      e foreach { pos =>
        pos match {
          case Some(_) => print("X")
          case None => print(" ")
        }
      }
      print("\n")
    }
  }
   
}
package com.helloworldopen.robotkeke



object Types {
  
  type Position = (Int, Int)
  type Velocity = (Int, Int)
  type State = (Position, Velocity)


  type Course = Array[Array[List[State]]]

  // Explored States
  type Course2 = Map[Position, List[State]]
}